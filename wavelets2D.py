#!/usr/bin/python3

import sys, os
import numpy as np
import scipy.io as io
import scipy.signal as signal
import matplotlib
import matplotlib.pyplot as plt
import time, datetime
import itertools
from mpl_toolkits.mplot3d import Axes3D

###############################################################
### The Douglas-Rachford algorithm and projection operators ###
###############################################################

### The Douglas-Rachford algorithm and projection operators ###
def DR(M,d,MAX_ITER=1e6,EPS=1e-6,initialU=0):
  """ Try to construct a wavelet ensemble using the DR algorithm.

      Parameters
      ----------
          M : int
            The length of the ensemble to be constructed.
          d : int
            The desired smooth of the ensemble to be constructed.
          MAX_ITER : int, optional
                   The maximum number of iterations to the DR algorithm will run.
                   If this is not specified, 1e6 is used. 
          EPS : float, optional
              The tolerance for the termination criterion (ie d(x_k,x_{k+1})<EPS)
              If this is not specified, 1e-6 is used. 
          initialU : np.array, optional 
                   The starting point for by the DR algorithm. If this is not
                   specified, the algorithm will be initialised with an array of
                   all zeros.

      Returns
      ----------
          U : np.array
            A matrix ensemble. If the algorithm terminated successfully, this will be
            a wavelet ensemble upto the specified numerical accuracy. 
          stats : list
                This is a list of the form wiht statistics from the computation. It is
                of the form [iterations, final_error, total_time, successful]
  """
   
  print( "#"*15 + "   Running DR    " + "#"*15 )
  assert(M+1>d)
  assert(M%2==0) 
  #initial point 
  #ensemble samples are indexed in the later two indices
  U = np.zeros((5,M,M,4,4),dtype="complex")
  for j in range(5):
    U[j,:,:,:,:] = initialU
  #main loop
  t0  = time.time()
  PDU = PD(U)
  err = np.inf
  it  = 0 #iteration counter
  PC5.first_call = True
  PC5.d          = d
  while (err > EPS) and (it < MAX_ITER):
    PCU = PC(2*PDU-U)
    TU  = U + PCU - PDU 
    err = np.linalg.norm(TU-U) 
    U   = TU
    PDU = PD(U)
    it += 1
    if ( it < 1000 and it % 100 == 0) or it % 1000 == 0:
      t = time.time() - t0
      print( "Iterations: %4d   Change: %.3E   Time: %.3fs" % (it,err,t)  )
  t   = time.time() - t0
  sol = PDU[0,:,:,:,:] 
  print( "#"*15 + "  DR Terminated  " + "#"*15 )  
  print( "Iterations:   ", it)
  print( "Change:        %.3E" % err)
  print( "Time:          %.3f seconds" %t) 
  print( "Termination:  ", "Convergence" if err < EPS else "Iteration limit ("+str(MAX_ITER)+") reached")
  print( "J-Condition:  ", checkJCondition(sol,EPS=EPS) )
  print( "Orth-shifts:  ", checkOrthShift(sol,EPS=EPS) ) 
  print( "Non-separable:", checkNonseparability(sol,EPS=EPS) )
  print( "Constraint 1: ", np.linalg.norm(sol-PC1(sol)) < EPS )
  print( "Constraint 2: ", np.linalg.norm(sol-PC2(sol)) < EPS )
  print( "Constraint 3: ", np.linalg.norm(sol-PC3(sol)) < EPS )
  print( "Constraint 4: ", np.linalg.norm(sol-PC4(sol)) < EPS )
  print( "Constraint 5: ", np.linalg.norm(sol-PC5(sol)) < EPS )
  print( "#"*47 )  

  stats = [it, err, t, err < EPS]
  return sol, stats

def PD(U):
  """ Computes the projection of a 3-tuple of matrix ensemble U onto the diagonal
      constraint D.

      Parameters
      ----------
         U : np.array
           A 3-tuple of matrix ensembles.

      Returns
      ---------
         V : np.array
           A 3-tuple of matrix ensembles which is the projection onto D.
 
  """
  V = np.mean(U,axis=0)
  V = np.repeat(V[np.newaxis,:,:,:,:],5,axis=0)
  return V

def PC(U):
  """ Computes the projection of a 3-tuple of matrix ensemble U onto the constraint
      C which is the product of C1, C2 and C3.
      

      Parameters
      ----------
         U : np.array
           A 3-tuple of matrix ensembles.

      Returns
      ---------
         V : np.array
           A 3-tuple of matrix ensembles which is the projection onto C.
 
  """
  V = np.zeros_like(U,dtype="complex")
  V[0,:,:,:,:] = PC1(U[0,:,:,:,:])
  V[1,:,:,:,:] = PC2(U[1,:,:,:,:])
  V[2,:,:,:,:] = PC3(U[2,:,:,:,:])
  V[3,:,:,:,:] = PC4(U[3,:,:,:,:])
  V[4,:,:,:,:] = PC5(U[4,:,:,:,:])
  return V

def PC1(U):
  """ Computes the projection of U, a matrix ensemble, onto C_1. """
  M                 = U.shape[0]
  V                 = np.zeros_like(U,dtype="complex")
  V[0,0,:,:]        = projU0(U[0,0,:,:])
  sigma             = np.array([[0,1,0,0],[1,0,0,0],[0,0,0,1],[0,0,1,0]])
  tau               = np.array([[0,0,1,0],[0,0,0,1],[1,0,0,0],[0,1,0,0]])
  V[M//2,0,:,:]     = np.matmul(sigma,V[0,0,:,:])
  V[0,M//2,:,:]     = np.matmul(tau,V[0,0,:,:])
  V[M//2,M//2,:,:,] = np.matmul(np.matmul(tau,sigma),V[0,0,:,:])
  for i,j in itertools.product(range(0,M//2),range(0,M//2)):
    if i != 0 or j != 0:
      V[i,j,:,:]            = nearestUnitary(U[i,j,:,:])
      V[i+M//2,j,:,:]       = np.matmul(sigma,V[i,j,:,:])
      V[i,j+M//2,:,:]       = np.matmul(tau,V[i,j,:,:])
      V[i+M//2,j+M//2,:,:]  = np.matmul(np.matmul(tau,sigma),V[i,j,:,:])
  return V 

def PC2(U):
  """ Computes the projection of a matrix ensemble U onto the orthogonal
      shift (in the p-direction) constraint, C2.
      

      Parameters
      ----------
         U : np.array
           A matrix ensemble. 

      Returns
      ---------
         V : np.array
           A matrix ensembe which is the projection of U. 
 
  """
  M = U.shape[0]
  p = np.array([1./(2.*M),0])
  V = modulate(U,+p)
  V = P_unitary(V)
  V = modulate(V,-p)
  return V


def PC3(U):
  """ Computes the projection of a matrix ensemble U onto the orthogonal
      shift (in the q-direction) constraint, C3.
      
      

      Parameters
      ----------
         U : np.array
           A matrix ensemble. 

      Returns
      ---------
         V : np.array
           A matrix ensembe which is the projection of U. 
 
  """
  M = U.shape[0]
  q = np.array([0,1./(2.*M)])
  V = modulate(U,+q)
  V = P_unitary(V)
  V = modulate(V,-q)
  return V

def PC4(U):
  """ Computes the projection of a matrix ensemble U onto the orthogonal
      shift (in the (p+q)-direction) constraint, C4.
      
      

      Parameters
      ----------
         U : np.array
           A matrix ensemble. 

      Returns
      ---------
         V : np.array
           A matrix ensembe which is the projection of U. 
 
  """
  M = U.shape[0]
  pq = np.array([1./(2.*M),1./(2.*M)])
  V = modulate(U,+pq)
  V = P_unitary(V)
  V = modulate(V,-pq)
  return V

def PC5(U):
  """ Computes the projection of a matrix ensemble U onto the regularity constraint C5.
      

      Parameters
      ----------
         U : np.array
           A matrix ensemble. 

      Returns
      ---------
         V : np.array
           A matrix ensembe which is the projection of U. 
 
  """
  M = U.shape[0]
  d = PC5.d
  V = U.copy()

  if PC5.first_call:
    #The matrix R_hash as defined in David's thesis
    R_hash = [] 
    for a1,a2 in itertools.product(range(0,d+1),range(0,d+1)):
       if 0 < a1+a2 and a1+a2 <= d: #|a|=a_1+a_2 in    
          R_hash.append( [k1**a1 * k2**a2 * (-1)**k1 for k1 in range(M) for k2 in range(M)] )                       
          R_hash.append( [k1**a1 * k2**a2 * (-1)**k2 for k1 in range(M) for k2 in range(M)] ) 
          R_hash.append( [k1**a1 * k2**a2 * (-1)**(k1+k2) for k1 in range(M) for k2 in range(M)] ) 
    R_hash = np.array(R_hash,dtype="complex")
    PC5.R_hash = R_hash
  
    #PC5.P_hash = np.eye(M**2) - R_hash.conj().T @ np.linalg.inv(R_hash @ R_hash.conj().T) @ R_hash
  
    #The matrix R as defined in David's thesis
    R = [] 
    for a1,a2 in itertools.product(range(0,d+1),range(0,d+1)):
       if 0 < a1+a2 and a1+a2 <= d: #|a|=a_1+a_2=<d   
          R.append( [k1**a1 * k2**a2 for k1 in range(M) for k2 in range(M)] )
    R = np.array(R,dtype="complex")
    PC5.R = R
  
    #PC5.P = np.eye(M**2) - R.conj().T @ np.linalg.inv(R@ R.conj().T) @ R

    PC5.first_call = False
  
  # Do the individual steps with fft
  A = myfft(U)

  #A[:,:,0,0] = np.matmul(PC5.P_hash,A[:,:,0,0].reshape(M**2)).reshape((M,M))
  Y = np.linalg.solve( np.matmul(PC5.R_hash,PC5.R_hash.conj().T), np.matmul(PC5.R_hash,A[:,:,0,0].reshape(M**2)) )
  A[:,:,0,0] -= np.matmul( PC5.R_hash.conj().T, Y).reshape((M,M))   

  #A[:,:,0,1] = np.matmul(PC5.P,A[:,:,0,1].reshape(M**2)).reshape((M,M))
  Y = np.linalg.solve( np.matmul(PC5.R,PC5.R.conj().T), np.matmul(PC5.R,A[:,:,0,1].reshape(M**2)) )
  A[:,:,0,1] -= np.matmul( PC5.R.conj().T, Y).reshape((M,M))   

  #A[:,:,0,2] = np.matmul(PC5.P,A[:,:,0,2].reshape(M**2)).reshape((M,M))
  Y = np.linalg.solve( np.matmul(PC5.R,PC5.R.conj().T), np.matmul(PC5.R,A[:,:,0,2].reshape(M**2)) )
  A[:,:,0,2] -= np.matmul( PC5.R.conj().T, Y).reshape((M,M))   

  #A[:,:,0,3] = np.matmul(PC5.P,A[:,:,0,3].reshape(M**2)).reshape((M,M))
  Y = np.linalg.solve( np.matmul(PC5.R,PC5.R.conj().T), np.matmul(PC5.R,A[:,:,0,3].reshape(M**2)) )
  A[:,:,0,3] -= np.matmul( PC5.R.conj().T, Y).reshape((M,M))   

  V = myifft(A)
    
  #Fill remaining rows so the two lemmas are satisfied
  V[:,:,1,:] = np.roll(V[:,:,0,:],M//2,0)
  V[:,:,2,:] = np.roll(V[:,:,0,:],M//2,1)
  V[:,:,3,:] = np.roll(V[:,:,1,:],M//2,1)    
    
  return V


      
###############################################################
###                   Helper functions                      ###
###############################################################
def P_unitary(U):
  """ Computes a projection of U, a matrix ensemble, onto the ensembles of untiary matrices
      satisfying the J-condition.

      Parameters
      ----------
         U : np.array
           A matrix ensemble. 

      Returns
      ---------
         V : np.array
           A matrix ensembe which is the projection of U. 
 
  """
  M      = U.shape[0]
  V      = np.zeros_like(U)
  sigma  = np.array([[0,1,0,0],[1,0,0,0],[0,0,0,1],[0,0,1,0]])
  tau    = np.array([[0,0,1,0],[0,0,0,1],[1,0,0,0],[0,1,0,0]])
  for i,j in itertools.product(range(0,M//2),range(0,M//2)):
    V[i,j,:,:]            = nearestUnitary(U[i,j,:,:])
    V[i+M//2,j,:,:]       = np.matmul(sigma,V[i,j,:,:])
    V[i,j+M//2,:,:]       = np.matmul(tau,V[i,j,:,:])
    V[i+M//2,j+M//2,:,:]  = np.matmul(np.matmul(tau,sigma),V[i,j,:,:])
  return V 

def myfft(U):
  """ This is a wrapper for numpy's 2-dimensional fft.

      Parameters
      ----------
         U : np.array
           A matrix ensemble no in the Fourier domain.

      Returns
      ---------
         A : np.array
           A matrix ensemble which is the Fourier transform of U.
  """
  A = np.fft.fftn(U,axes=(0,1),norm="ortho")
  return A

def myifft(A):
  """ This is a wrapper for numpy's 2-dimensional ifft.

      Parameters
      ----------
         A : np.array
           A matrix ensemble in the Fourier domain.

      Returns
      ---------
         U : np.array
           A matrix ensemble which is the inverse Fourier transform of A.
  """
  U = np.fft.ifftn(A,axes=(0,1),norm="ortho")
  return U

def trigPoly(U,xi):
  """ Returns the trigonmetric polynomial p generated from U
      evaluated at the point xi. Here p is given by
         p(xi) = \sum_{k} A_k exp(2*\pi*i*k*xi)
      where A=(A_k) is the Fourier transform of the ensemble U=(U_k).

      Parameters
      ----------
         U : np.array 
           A matrix ensemble.
         xi : float
            The point at which the polynomial is to be evaluated.

      Returns
      ---------
         p_xi : np.array
              The value of the trigonmetric polynomial of U at xi.
  """
  M    = U.shape[0]
  A    = myfft(U)
  #p_xi = np.sum([A[k,:,:]*np.exp(2.*np.pi*1.j*k*xi) for k in range(M)],axis=0)
  #p_xi = np.einsum('i...,i',A,np.array([np.exp(2.*np.pi*1.j*k*xi) for k in range(M)]))
  e = np.array([[np.exp(2.*np.pi*1.j*np.dot(np.array([k1,k2]),xi)) for k2 in range(M)]
                                                                   for k1 in range(M)])
  p_xi = np.sum(A*e[:,:,np.newaxis,np.newaxis],axis=(0,1)) 
  #print(np.around(U[1,1,:,:]))
  #print()
  #print(np.around(p_xi/M,2))
  return p_xi / M

def trigPolyA(A,xi):
  """ Returns the trigonmetric polynomial p generated from A=fft(U) 
      evaluated at the point xi. Here p is given by
         p(xi) = \sum_{k} A_k exp(2*\pi*i*k*xi)
      where A=(A_k) is the Fourier transform of the ensemble U=(U_k).

      Parameters
      ----------
         A : np.array 
           A matrix ensemble which is the Fourier transform of an ensemble U.
         xi : float
            The point at which the polynomial is to be evaluated.

      Returns
      ---------
         p_xi : np.array
              The value of the trigonmetric polynomial of U at xi.
  """
  M    = A.shape[0]
  #A    = myfft(U)
  #p_xi = np.sum([A[k,:,:]*np.exp(2.*np.pi*1.j*k*xi) for k in range(M)],axis=0)
  #p_xi = np.einsum('i...,i',A,np.array([np.exp(2.*np.pi*1.j*k*xi) for k in range(M)]))
  e = np.array([[np.exp(2.*np.pi*1.j*np.dot(np.array([k1,k2]),xi)) for k2 in range(M)]
                                                                   for k1 in range(M)])
  p_xi = np.sum(A*e[:,:,np.newaxis,np.newaxis],axis=(0,1)) 
  #print(np.around(U[1,1,:,:]))
  #print()
  #print(np.around(p_xi/M,2))
  return p_xi / M

def modulate(U,shift):
  """ Generate a matrix ensemble by applying the modulation operator with a specified
      shift to the ensemble U.

      Parameters
      ----------
         U : np.array
           A matrix ensemble. 
         shift : np.array
               The modulation operator will use this shift.

      Returns
      ----------
         V : np.array
           The matrix ensemble obtained after modulation with the specified shift.

  """
  V = np.zeros_like(U,dtype="complex")
  M = U.shape[0]
  A = myfft(U)
  for i, j in itertools.product(range(M),range(M)):
    V[i,j,:,:] = trigPolyA(A,np.array([float(i),float(j)])/M+shift)
  return V

def nearestUnitary(X):
  """ Computes a nearest orthogonal matrix to X using an SVD in the Frobenius norm.

      Parameters
      ----------
         X : np.array
           A matrix.

      Returns
      ----------
         V : np.array
           An orthogonal matrix nearest to X.

  """
  u, _, vh = np.linalg.svd(X)
  return np.dot(u,vh)

def projU0(X):
  """ Computing the projection of a matrix X onto the first coordinate of the
      constraint C1. 

      Parameters
      ----------
         X : np.array
           A matrix. Typically this will be the first coordinate of a matrix
           ensemble U.

      Returns
      ----------
         V : np.array
           The projection of X onto the first coordinate of C1.

  """
  V        = np.zeros_like(X)
  V[0,0]   = 1.
  V[1:,1:] = nearestUnitary(X[1:,1:])
  return V
   
def daub1d(d):
  """ Generates the matirx ensemble U associated with the 
      1D Daubechies wavelet of smoothness d.

      Parameters
      ----------
         d : int
           A positive integer representing the smoothness of the wavelet.

      Returns
      ----------
         U : np.array
           The matrix ensemble coresponding to the Daubechie wavelet of 
           smoothness d. 

  """
  x  = signal.daub(d+1) / np.sqrt(2)
  M  = len(x)
  y  = np.flip(x,axis=0) * np.array([(-1)**n for n in range(M)])
  m0 = np.zeros(M,dtype="complex")
  m1 = np.zeros(M,dtype="complex") 
  for j in range(M):
    m0[j] = sum( [x[k]*np.exp(2.j*np.pi*j*k/M) for k in range(M)] )
    m1[j] = sum( [y[k]*np.exp(2.j*np.pi*j*k/M) for k in range(M)] )
  #m0 /= np.sqrt(d+1)
  #m1 /= np.sqrt(d+1)
  U = np.zeros((M,2,2),dtype="complex")
  U[:,0,0] = m0
  U[:,0,1] = m1
  U[:,1,0] = np.roll(m0,M//2)
  U[:,1,1] = np.roll(m1,M//2)
  return U 

def daub_ms(d):
  """ A helper function for the 2d daub function. This generates the m0 and m1.

      Parameters
      ----------
         d : int
           A positive integer representing the smoothness of the wavelet.

      Returns
      ----------
         m0 : np.array
            The top-left entries of Daubechie matrix ensemble.
         m1 :  np.array
            The top-right entries of Daubechie matrix ensemble.
        

  """
  x  = signal.daub(d+1) / np.sqrt(2)
  M  = len(x)
  y  = np.flip(x,axis=0) * np.array([(-1)**n for n in range(M)])
  m0 = np.zeros(M,dtype="complex")
  m1 = np.zeros(M,dtype="complex") 
  for j in range(M):
    m0[j] = sum( [x[k]*np.exp(2.j*np.pi*j*k/M) for k in range(M)] )
    m1[j] = sum( [y[k]*np.exp(2.j*np.pi*j*k/M) for k in range(M)] )
  return [m0,m1]


def daub(d):
  """ Generates the matirx ensemble U associated with the 
      2D Daubechies wavelet of smoothness d via a tensor product.

      Parameters
      ----------
         d : int
           A positive integer representing the smoothness of the wavelet.

      Returns
      ----------
         U : np.array
           The matrix ensemble coresponding to the Daubechie wavelet of 
           smoothness d. 

  """
  [m0,m1]    = daub_ms(d) 
  M          = len(m0)
  m0m0       = np.outer(m0,m0)
  m1m0       = np.outer(m1,m0) 
  m0m1       = np.outer(m0,m1)
  m1m1       = np.outer(m1,m1)
  U          = np.zeros((M,M,4,4),dtype="complex")
  U[:,:,0,:] = np.stack([m0m0,m1m0,m0m1,m1m1],axis=-1)
  U[:,:,1,:] = np.roll(U[:,:,0,:],M//2,axis=0)
  U[:,:,2,:] = np.roll(U[:,:,0,:],M//2,axis=1)
  U[:,:,3,:] = np.roll(U[:,:,2,:],M//2,axis=0)
  return U


###############################################################
###        Functions used for processing the results        ###
###############################################################

def checkJCondition(U,EPS=1e-6):
  """ Numerically Checks if the matrix ensemble U satisifes the generalisation
      of the J-condition to two dimenions.

      Parameters
      ----------
         U : np.array
           A matrix ensemble.
         EPS : float, optional
             The threshold to be used in the numerical check. If this is not
             specified, then 1e-6 is used.

      Returns
      ----------
         p  : bool
            A boolean variable which takes the value True if U passed the check,
            and False otherwise.   
  """
  M     = U.shape[0]
  sigma = np.array([[0,1,0,0],[1,0,0,0],[0,0,0,1],[0,0,1,0]])   
  tau   = np.array([[0,0,1,0],[0,0,0,1],[1,0,0,0],[0,1,0,0]])
  err  = sum([ np.linalg.norm(sigma @ U[M//2+j,i,:,:] - U[j,i,:,:]) 
                                   for j,i in zip(range(M//2),range(M//2)) ]) 
  err += sum([ np.linalg.norm(tau @ U[j,M//2+i,:,:] - U[j,i,:,:]) 
                                   for j,i in zip(range(M//2),range(M//2)) ])
  err += sum([ np.linalg.norm(sigma @ tau @ U[M//2+j,M//2+i,:,:] - U[j,i,:,:])   
                                   for j,i in zip(range(M//2),range(M//2)) ])
  p = err < EPS
  return p

def checkOrthShift(U,EPS=1e-6):
  """ Numerically checks if the matrix ensemble U gives rise to orthogonal shifts.

      Parameters
      ----------
         U : np.array
           A matrix ensemble.
         EPS : float, optional
             The threshold to be used in the numerical check. If this is not
             specified, then 1e-6 is used.

      Returns
      ----------
         p  : bool
            A boolean variable which takes the value True if U passed the check,
            and False otherwise.
  """
  M          = U.shape[0]
  xi         = np.linspace(-0.25,0.25,num=2*M)
  yi         = np.linspace(-0.25,0.25,num=2*M)
  m0         = np.array([trigPoly(U,np.array([x,y]))[0,0] for x in xi for y in yi])
  p = np.min(abs(m0)) > EPS 
  return p
  
def checkNonseparability(U,EPS=1e-6):
  """ Numerically checks if a matrix ensemble is nonseparable using a sufficient
      criteria.

      Parameters
      ----------
         U : np.array
           A matrix ensemble.
         EPS : float, optional
             The threshold to be used in the numerical check. If this is not
             specified, then 1e-6 is used.

      Returns
      ----------
         p  : bool
            A boolean variable which takes the value True if U passed the check,
            and False otherwise.
  """
  M = U.shape[0]
  t = M//2
  m = np.zeros((4,M,M),dtype="complex")
  for i,j in itertools.product(range(M),range(M)):
    m[0,i,j] = np.linalg.norm( U[i,j,0,0]-U[0,j,0,0]*U[i,0,0,0]/U[0,0,0,0] ) 
    m[1,i,j] = np.linalg.norm( U[i,j,0,1]-U[t,j,0,1]*U[i,0,0,1]/U[t,0,0,1] ) 
    m[2,i,j] = np.linalg.norm( U[i,j,0,2]-U[0,j,0,2]*U[i,t,0,2]/U[0,t,0,2] ) 
    m[3,i,j] = np.linalg.norm( U[i,j,0,3]-U[t,j,0,3]*U[i,t,0,3]/U[t,t,0,3] )  
  p = np.linalg.norm(m) > EPS
  return p



def ensembleToWaveletPair(U):
  """ Converts a matrix ensemble to a wavlet pair, Phi and Psi, in readness for 
      applying the Cascade algorithm. This code is a direct translation of
      David Franklin's PhD code.

      Parameters
      ----------
         U : np.array
           A matrix ensemble.

      Returns
      ----------
         Phi : np.array
            The values of the scaling function coresponding to U at the points specified
            by X1 and X2.
         Psi : np.array
            The values of the wavelet coresponding to U at the points specified  byX1 and X2.
         X1 : array_like
           The first coordinate where the function have been evaluated.
         X2 : array_like
           The second coordinate where the function have been evaluated.
  """

  M = U.shape[0]
  A = myfft(U)[:,:,0,:]/M

  #Cascade Skeleton
  B=np.zeros((M**2,M**2),dtype='complex')
  for x1,x2,k1,k2 in itertools.product(range(1,M+1),range(1,M+1),range(1,M+1),range(1,M+1)):
    X1 = 2*x1-k1
    X2 = 2*x2-k2
    if X1>0 and X1<M+1 and X2>0 and X2<M+1:
        B[(x2-1)*M+x1-1,(X2-1)*M+X1-1] = A[k1-1,k2-1,0]      

  
  d,V = np.linalg.eig(B)        
  y = int(np.where(d.real>0.249)[-1])
  Phi = V[:,y].reshape((M,M))
  X1,X2 = np.meshgrid(np.linspace(0,M-1,M),np.linspace(0,M-1,M))

   
  Psi0,X1,X2 = cascPhi(Phi,X1,X2,A[:,:,0],5)
  m = Psi0.shape[-1]
  Psi = np.zeros((M,m,m),dtype='complex')
  Psi[0,:,:] = Psi0
  Psi[1,:,:],X1,X2 = cascPsi(Psi0,X1,X2,A[:,:,1])
  Psi[2,:,:],X1,X2 = cascPsi(Psi0,X1,X2,A[:,:,2])
  Psi[3,:,:],X1,X2 = cascPsi(Psi0,X1,X2,A[:,:,3])

  Phi = Psi[0,:,:]
  Psi = Psi[1:,:,:]

  return Phi, Psi, X1, X2
  
def ensembleToCoeff(U):
  """ Extract the coefficients of m_0 from a matrix ensemble.

      Parameters
      ----------
         U : np.array
           A matrix ensemble.

      Returns
      ----------
         m_0 : np.array
           Coefficients of the scaling function m_0
  """

  M = U.shape[0]
  d = M//2 - 1
  return np.fft.fftn(U[:,:,0,0],axes=(0,1),norm="ortho") / np.sqrt(d+1)

def cascPhi(Phi,X1,X2,A0,n):
    """ This is a helper function for the function ensembleToWaveletPair. 
        This code is a direct translation of David Franklin's PhD code.
    """
    M = A0.shape[0]
    X=X1[0,:]
    
    for i in range(n):
        step = X[1]-X[0]
        Xn   = np.linspace(X[0],X[-1],2*len(X)-1)
        m    = len(Xn)
        
        Phin = np.zeros((m,m),dtype='complex')
        for i1,i2 in itertools.product(range(m),range(m)):
            x1,x2 = Xn[i1],Xn[i2]
            
            xo1,xo2 = np.argwhere(X==x1),np.argwhere(X==x2)
            
            if len(xo1)>0 and len(xo2)>0:
                Phin[i1,i2] = Phi[xo1[0,0],xo2[0,0]]
            else:
                for k1,k2 in itertools.product(range(M),range(M)):
                    xo1,xo2 = np.argwhere(X==2*x1-k1),np.argwhere(X==2*x2-k2)
                    
                    if len(xo1)>0 and len(xo2)>0:
                        #Phin[i1,i2] += 4*A0[k1,k2]*Phi[xo1,xo2]
                        Phin[i1,i2] += 4*A0[k2,k1]*Phi[xo1,xo2]
            
        X=Xn
        Phi=Phin
    
    X1,X2 = np.meshgrid(X,X)
        
    return Phi,X1,X2  


def cascPsi(Phi,X1,X2,Ak):
    """ This is a helper function for the function ensembleToWaveletPair. 
        This code is a direct translation of David Franklin's PhD code.
    """
    M = Ak.shape[0]
    X = X1[0,:]
    m = len(X)
    Psi = np.zeros((m,m),dtype='complex')
    for i1,i2,k1,k2 in itertools.product(range(m),range(m),range(M),range(M)):
        x1,x2 = X[i1],X[i2]
        xo1,xo2 = np.argwhere(X==2*x1-k1),np.argwhere(X==2*x2-k2)
        if len(xo1)>0 and len(xo2)>0:
            #Psi[i1,i2] += 4*Ak[k1,k2]*Phi[xo1,xo2]
            Psi[i1,i2] += 4*Ak[k2,k1]*Phi[xo1,xo2]
    return Psi,X1,X2



def plotWaveletPair2D(U,filename=False):
   """ Plots the scaling and wavelet functions generated by the matrix ensemble U.
      This function is used to generate the plots in the paper.
      
      Parameters
      ----------
         U : np.array
           The matrix ensemble from which the functions will be generated.
         filename  : str, optional
             If specificed, the plot will saved using the specified
             filename, else the plot will just be displayed.
   """
   Phi, Psi, X1, X2 = ensembleToWaveletPair(U)
   fig=plt.figure()

   v_max = max([np.amax(Phi.imag),np.amax(Psi.imag)])
   v_min = min([np.amin(Phi.imag),np.amin(Psi.imag)]) 

   minn, maxx = v_min, v_max
   norm = matplotlib.colors.Normalize(minn, maxx)
   m = plt.cm.ScalarMappable(norm=norm, cmap='jet')
   m.set_array([])

   ax=fig.add_subplot(221, projection='3d',adjustable='box')
   fcolors = m.to_rgba(Phi.imag)
   ax.plot_surface(X1,X2,Phi.real,facecolors=fcolors, vmin=minn, vmax=maxx)
   ax.title.set_text('$\\Phi$ - Scaling Function')
   #ax.view_init(35,45)

   ax=fig.add_subplot(222, projection='3d')
   fcolors = m.to_rgba(Psi[0,:,:].imag)
   ax.plot_surface(X1,X2,Psi[0,:,:].real,facecolors=fcolors, vmin=minn, vmax=maxx)
   ax.title.set_text('$\\Psi_1$ - First Wavelet')
   #ax.view_init(35,45)

   ax=fig.add_subplot(223, projection='3d',adjustable='box')
   fcolors = m.to_rgba(Psi[1,:,:].imag)
   ax.plot_surface(X1,X2,Psi[1,:,:].real,facecolors=fcolors, vmin=minn, vmax=maxx)
   ax.title.set_text('$\\Psi_2$ - Second Wavelet')
   #ax.view_init(35,45)

   ax=fig.add_subplot(224, projection='3d',adjustable='box')
   fcolors = m.to_rgba(Psi[2,:,:].imag)
   im = ax.plot_surface(X1,X2,Psi[2,:,:].real,facecolors=fcolors, vmin=minn, vmax=maxx)
   ax.title.set_text('$\\Psi_3$ - Third Wavelet')
   #ax.view_init(35,45)



   fig.subplots_adjust(right=0.8,wspace=0.25,hspace=0.5)
   cbar_ax = fig.add_axes([0.85, 0.25, 0.05, 0.5])
   fig.colorbar(m, cax=cbar_ax)

   if not filename:
     plt.show()
   else:
     plt.savefig(filename)
   plt.close()

def plotTrigPoly2D(U,filename=False):
   """ Plots the trigonmetric polynomial generated by the matrix ensemble U. 
      This function is used to generate the plots in the paper.
      
      Parameters
      ----------
         U : np.array
           The matrix ensemble from which the functions will be generated.
         filename  : str, optional
             If specificed, the plot will saved using the specified
             filename, else the plot will just be displayed.
   """
   samples = 100
   X1,X2 = np.meshgrid(np.linspace(0,1.,samples),np.linspace(0,1.,samples))
   Z     = np.zeros((samples,samples,4),dtype='complex')
   for i in range(samples):
      for j in range(samples):
         Z[i,j,:] = trigPoly(U,np.array([X1[i,j],X2[i,j]]))[0,:]
 
   #cw   = matplotlib.cm.coolwarm
   #v_min = np.amin(Z.real)
   #v_max = np.amax(Z.real)

   color_dimension = Z.imag
   minn, maxx = color_dimension.min(), color_dimension.max()
   norm = matplotlib.colors.Normalize(minn, maxx)
   m = plt.cm.ScalarMappable(norm=norm, cmap='jet')
   m.set_array([])

   fig=plt.figure()
   ax=fig.add_subplot(221, projection='3d',adjustable='box')
   fcolors = m.to_rgba(color_dimension[:,:,0])
   ax.plot_surface(X1,X2,Z[:,:,0].real,facecolors=fcolors, vmin=minn, vmax=maxx)
   ax.title.set_text('$m_0$')

   ax=fig.add_subplot(222, projection='3d',adjustable='box')
   fcolors = m.to_rgba(color_dimension[:,:,1])
   ax.plot_surface(X1,X2,Z[:,:,1].real,facecolors=fcolors, vmin=minn, vmax=maxx)
   ax.title.set_text('$m_1$')
   #ax.view_init(35,45)

   ax=fig.add_subplot(223, projection='3d',adjustable='box')
   fcolors = m.to_rgba(color_dimension[:,:,2])
   ax.plot_surface(X1,X2,Z[:,:,2].real,facecolors=fcolors, vmin=minn, vmax=maxx)
   ax.title.set_text('$m_2$')
   #ax.view_init(35,45)

   ax=fig.add_subplot(224, projection='3d',adjustable='box')
   fcolors = m.to_rgba(color_dimension[:,:,3])
   im = ax.plot_surface(X1,X2,Z[:,:,3].real,facecolors=fcolors, vmin=minn, vmax=maxx)
   ax.title.set_text('$m_3$')
   #ax.view_init(35,45)

   fig.subplots_adjust(right=0.8,wspace=0.25,hspace=0.5)
   cbar_ax = fig.add_axes([0.85, 0.25, 0.05, 0.5])
   fig.colorbar(m,cax=cbar_ax)

   if not filename:
     plt.show()
   else:
     plt.savefig(filename)
   plt.close()
  

###############################################################
###           A example of the method in action             ###
###############################################################

def printCoeff( h ):
  print( "Coefficients of h are:" )
  print( h )

def paperExperiments(directory):
  """ Running this method produces the paper's results for the 2D problem. """  

  plt.style.use('grayscale')
  repetitions = 10
  parameters  = [(4,1),(6,2)] 
  start_seed  = 1951
  for (M,d) in parameters:
    s = start_seed
    #stats = [it, err, t]
    iters, error, time, solved = [],[],[],[]
    for r in range(repetitions):
      s += 1
      print("#"*47)
      print( "Problem dimensions are (M,d) = (" +str(M) + "," + str(d) + ")")
      print( "Initial Point Seed:", s )
      np.random.seed(s)
      U0 = PC1( 2.*np.random.rand(M,M,4,4)-1. + (2.*np.random.rand(M,M,4,4)-1.)*1.j )

      U, stats  = DR(M,d,EPS=1e-3,MAX_ITER=1e6,initialU=U0)
      h = ensembleToCoeff(U) 
      printCoeff(h)

      #Try to generate plots
      name = "s"+str(s)
      io.savemat(directory+"/2D-M"+str(M)+"d"+str(d)+"-"+name+"-ensemble.mat",{'U':U})
      try:
        plotTrigPoly2D(U,filename=directory+"/2D-M"+str(M)+"d"+str(d)+"-"+name+"-trigpoly.pdf")
        plotWaveletPair2D(U,filename=directory+"/2D-M"+str(M)+"d"+str(d)+"-"+name+"-wavelet.pdf")
      except:
        print("\n *** Could not generate plots (I probably couldn't find an eigenvalue > 0.25) *** ")

      iters.append(stats[0])
      error.append(stats[1])
      time.append(stats[2])
      solved.append(stats[3])

      print("")
   

    print( "#"*47 )  
    print( "#"*14 + "  Instance Summary  " + "#"*13 )  
    print( "  Dimensions are (M,d)  = ("+str(M)+","+str(d)+")" )

    print( "  Instances solved      =", sum(solved), "of" , repetitions )
    iters = list(itertools.compress(iters,solved))
    if iters != []:
      print( "  Mean (max) iterations = %.3f (%.3f)" % (np.mean(iters), np.max(iters)) )
    time  = list(itertools.compress(time,solved))
    if time != []:
      print( "  Mean (max) time       = %.3f (%.3f)" % (np.mean(time), np.max(time)) )
    print( "#"*47 )  
    print( "#"*47 )  
    print( "\n\n\n" )


def main():
  (M,d) = (4,1)
  print("#"*47)
  print( "Problem dimensions are (M,d) = (" +str(M) + "," + str(d) + ")")
  U,_  = DR(M,d)
  h = ensembleToCoeff(U) 
  printCoeff(h)
  plotTrigPoly2D(U) 
  plotWaveletPair2D(U) 



if __name__ == '__main__':
  if len(sys.argv) > 1 and sys.argv[1] == "-e":
    directory = "Results2D-" + datetime.date.today().strftime("%b%d")
    if not os.path.exists(directory):
      os.makedirs(directory)
    paperExperiments(directory)
  else:
    main()
