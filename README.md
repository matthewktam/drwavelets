# **DRwavelets**: A numerical approach to constructing non-separable wavelets

This repository contains the source code for the manuscript:

>**D. Franklin**, **J.A. Hogan** and **M.K. Tam** (2020).  
A Douglas-Rachford construction of non-separable continuous compactly supported
multidimensional wavelets.  

This work is partly based on the first author's PhD thesis which you can download from [here](https://nova.newcastle.edu.au/vital/access/manager/Repository/uon:33802). Contributions to the code were also made by **Neil Dizon** (University of Newcastle).


## Dependences and structure

This `python3` code requires the following libraries:

>`scipy` `numpy` `matplotlib`

The repository contains the following files:
* `wavelets1D.py` - code for the 1D wavelet construction problem.
* `wavelets2D.py` - code for the 2D wavelet construction problem.
* `Demo.ipynb` - a Jupyter notebook containing a short demo of the code.
* `Plots.ipynb` - a Jupyter notebook containing the plots shown in the manuscript.


## Demo
The simplest way to see the code in action is the open `Demo.ipynb` in Jupyter. The following nbviewer link shows the results of running this notebook:
[Jupyter notebook - DRwavelets Demo](https://nbviewer.jupyter.org/urls/gitlab.com/matthewktam/drwavelets/-/raw/master/Demo.ipynb)

A similar demo can also be run in the command line:
* `python wavelet1D.py` - runs the 1D wavelet demo.
* `python wavelet2D.py` - runs the 2D wavelet demo.

## Experiments
The experimental results which appear in the manuscript can be reproduced by appending the `-e` flag to the Python code in the command line. That is,
* `python wavelet1D.py -e` - runs the 1D wavelet experiments.
* `python wavelet2D.py -e` - runs the 2D wavelet experiments.

The plots for the figures shown in the manuscript are shown in `Plots.ipynb`. The corresponding nbviewer link is: [Jupyter notebook - Plots](https://nbviewer.jupyter.org/urls/gitlab.com/matthewktam/drwavelets/-/raw/master/Plots.ipynb)


## Licensing
[![CC BY 4.0][cc-by-image]][cc-by]

This work is licensed under a [Creative Commons Attribution 4.0 International
License][cc-by].

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-image]: https://i.creativecommons.org/l/by/4.0/88x31.png
