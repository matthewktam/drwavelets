#!/usr/bin/python3

import sys, os
import numpy as np
import scipy.io as io
import scipy.signal as signal
import matplotlib.pyplot as plt
import time, datetime
from itertools import compress
#from mpmath import mp, fp

###############################################################
### The Douglas-Rachford algorithm and projection operators ###
###############################################################

def DR(M,d,MAX_ITER=1e6,EPS=1e-6,initialU=0):
  """ Try to construct a wavelet ensemble using the DR algorithm.

      Parameters
      ----------
          M : int
            The length of the ensemble to be constructed.
          d : int
            The desired smooth of the ensemble to be constructed.
          MAX_ITER : int, optional
                   The maximum number of iterations to the DR algorithm will run.
                   If this is not specified, 1e6 is used. 
          EPS : float, optional
              The tolerance for the termination criterion (ie d(x_k,x_{k+1})<EPS)
              If this is not specified, 1e-6 is used. 
          initialU : np.array, optional 
                   The starting point for by the DR algorithm. If this is not
                   specified, the algorithm will be initialised with an array of
                   all zeros.

      Returns
      ----------
          U : np.array
            A matrix ensemble. If the algorithm terminated successfully, this will be
            a wavelet ensemble upto the specified numerical accuracy. 
          stats : list
                This is a list of the form wiht statistics from the computation. It is
                of the form [iterations, final_error, total_time, successful]
  """
   
  print( "#"*15 + "   Running DR    " + "#"*15 )
  assert(M+1>d)
  assert(M%2==0) 
  #nearestUnitary.use_mpmath = False #compute svd with arbitrary precision?
  #initial point 
  U = np.zeros((3,M,2,2),dtype="complex")
  for j in range(3):
    U[j,:,:,:] = initialU
  #main loop
  t0  = time.time()
  PDU = PD(U)
  err = np.inf
  it  = 0 #iteration counter
  PC3.first_call = True
  PC3.d = d
  while (err > EPS) and (it < MAX_ITER):
    PCU = PC(2*PDU-U)
    TU  = U + PCU - PDU 
    err = np.linalg.norm(TU-U) 
    U   = TU
    PDU = PD(U)
    it += 1
    if ( it < 1000 and it % 100 == 0) or it % 1000 == 0:
      t = time.time() - t0
      print( "Iterations: %4d   Change: %.3E   Time: %.3fs" % (it,err,t)  )
    #if nearestUnitary.use_mpmath == False and err < 5.3e-4:
    #  print("Increasing svd precision...")
    #  nearestUnitary.use_mpmath = True
  t   = time.time() - t0
  sol = PDU[0,:,:,:] 

  print( "#"*15 + "  DR Terminated  " + "#"*15 )  
  print( "Iterations:   ", it)
  print( "Change:        %.3E" % err)
  print( "Time:          %.3f seconds" %t) 
  print( "Termination:  ", "Convergence" if err < EPS else "Iteration limit ("+str(MAX_ITER)+") reached")
  print( "J-Condition:  ", checkJCondition(sol,EPS=EPS) )
  print( "Orth-shifts:  ", checkOrthShift(sol,EPS=EPS) ) 
  print( "Constraint 1: ", np.linalg.norm(sol-PC1(sol)) < EPS )
  print( "Constraint 2: ", np.linalg.norm(sol-PC2(sol)) < EPS )
  print( "Constraint 3: ", np.linalg.norm(sol-PC3(sol)) < EPS )
  print( "#"*47 )  
  stats = [it, err, t, err < EPS]

  return sol, stats

def PD(U):
  """ Computes the projection of a 3-tuple of matrix ensemble U onto the diagonal
      constraint D.

      Parameters
      ----------
         U : np.array
           A 3-tuple of matrix ensembles.

      Returns
      ---------
         V : np.array
           A 3-tuple of matrix ensembles which is the projection onto D.
 
  """
  V = np.mean(U,axis=0)
  V = np.repeat(V[np.newaxis,:,:,:],3,axis=0)
  return V

def PC(U):
  """ Computes the projection of a 3-tuple of matrix ensemble U onto the constraint
      C which is the product of C1, C2 and C3.
      

      Parameters
      ----------
         U : np.array
           A 3-tuple of matrix ensembles.

      Returns
      ---------
         V : np.array
           A 3-tuple of matrix ensembles which is the projection onto C.
 
  """
  V = np.zeros_like(U,dtype="complex")
  V[0,:,:,:] = PC1(U[0,:,:,:])
  V[1,:,:,:] = PC2(U[1,:,:,:])
  V[2,:,:,:] = PC3(U[2,:,:,:])
  return V

def PC1(U):
  """ Computes the projection of a matrix ensemble U onto C1.
      

      Parameters
      ----------
         U : np.array
           A matrix ensemble. 

      Returns
      ---------
         V : np.array
           A matrix ensembe which is the projection of U. 
 
  """
  M           = U.shape[0]
  V           = np.zeros_like(U,dtype="complex")
  V[0,:,:]    = projU0(U[0,:,:])
  sigma       = np.array([[0,1],[1,0]])
  V[M//2,:,:] = np.matmul(sigma,V[0,:,:])
  for j in range(1,M//2):
    V[j,:,:]      = nearestUnitary(U[j,:,:])
    V[j+M//2,:,:] = np.matmul(sigma,V[j,:,:])
  return V 

def PC2(U):
  """ Computes the projection of a matrix ensemble U onto the orthogonal shift constraints C2.
      

      Parameters
      ----------
         U : np.array
           A matrix ensemble. 

      Returns
      ---------
         V : np.array
           A matrix ensembe which is the projection of U. 
 
  """
  M = U.shape[0]
  V = modulate(U,+1./(2.*M))
  V = P_unitary(V)
  V = modulate(V,-1./(2.*M))
  return V

def PC3(U):
  """ Computes the projection of a matrix ensemble U onto the regularity constraint C3.
      

      Parameters
      ----------
         U : np.array
           A matrix ensemble. 

      Returns
      ---------
         V : np.array
           A matrix ensembe which is the projection of U. 
 
  """
  M = U.shape[0]
  #d = M//2-1
  d = PC3.d

  #Pre-compute the matrices for the projection (don't compute inverse explicitly)
  if PC3.first_call:
    R     = np.array([[k**l for k in range(M)] for l in range(d+1)],dtype="complex")
    PC3.R = R
    PC3.RRconj = np.matmul(R,R.conj().T)
    #invRR = np.linalg.inv( np.matmul(R,R.conj().T) )
    #PC3.P = ( np.eye(M) - np.matmul(R.conj().T,np.matmul(invRR,R)) ).astype("complex")
    PC3.first_call = False
  #Apply the projection
  A = myfft(U)

  #A[:,0,1] = np.matmul(PC3.P,A[:,0,1])
  y        = np.linalg.solve(PC3.RRconj,np.matmul(PC3.R,A[:,0,1]))
  A[:,0,1] = A[:,0,1] - np.matmul(PC3.R.conj().T,y)
  #A[:,1,0] = np.matmul(PC3.P,A[:,1,0])
  y        = np.linalg.solve(PC3.RRconj,np.matmul(PC3.R,A[:,1,0]))
  A[:,1,0] = A[:,1,0] - np.matmul(PC3.R.conj().T,y)
  #A[:,0,0] = np.matmul(PC3.P,A[:,1,0]) * np.array([(-1)**k for k in range(M)])
  y        = np.linalg.solve(PC3.RRconj,np.matmul(PC3.R,A[:,1,0]))
  A[:,0,0] = (A[:,1,0] - np.matmul(PC3.R.conj().T,y))  * np.array([(-1)**k for k in range(M)])
  #A[:,1,1] = np.matmul(PC3.P,A[:,0,1]) * np.array([(-1)**k for k in range(M)])
  y        = np.linalg.solve(PC3.RRconj,np.matmul(PC3.R,A[:,0,1]))
  A[:,1,1] = (A[:,0,1] - np.matmul(PC3.R.conj().T,y)) * np.array([(-1)**k for k in range(M)])

  U = myifft(A)
  return U
###############################################################
###                   Helper functions                      ###
###############################################################
def P_unitary(U):
  """ Computes a projection of U, a matrix ensemble, onto the ensembles of untiary matrices
      satisfying the J-condition.

      Parameters
      ----------
         U : np.array
           A matrix ensemble. 

      Returns
      ---------
         V : np.array
           A matrix ensembe which is the projection of U. 
 
  """
  M = U.shape[0]
  V = np.zeros_like(U)
  for j in range(M//2):
    V[j,:,:]      = nearestUnitary(U[j,:,:])
    V[j+M//2,:,:] = np.flip(V[j,:,:],0)
  return V 

def myfft(U):
  """ This is a wrapper for numpy's 1-dimensional fft.

      Parameters
      ----------
         U : np.array
           A matrix ensemble no in the Fourier domain.

      Returns
      ---------
         A : np.array
           A matrix ensemble which is the Fourier transform of U.
  """
  A = np.fft.fft(U,axis=0,norm="ortho")
  return A

def myifft(A):
  """ This is a wrapper for numpy's 1-dimensional ifft.

      Parameters
      ----------
         A : np.array
           A matrix ensemble in the Fourier domain.

      Returns
      ---------
         U : np.array
           A matrix ensemble which is the inverse Fourier transform of A.
  """
  U = np.fft.ifft(A,axis=0,norm="ortho")
  return U

def trigPoly(U,xi):
  """ Returns the trigonmetric polynomial p generated from U
      evaluated at the point xi. Here p is given by
         p(xi) = \sum_{k} A_k exp(2*\pi*i*k*xi)
      where A=(A_k) is the Fourier transform of the ensemble U=(U_k).

      Parameters
      ----------
         U : np.array 
           A matrix ensemble.
         xi : float
            The point at which the polynomial is to be evaluated.

      Returns
      ---------
         p_xi : np.array
              The value of the trigonmetric polynomial of U at xi.
  """
  M    = U.shape[0]
  A    = myfft(U)
  #p_xi = np.sum([A[k,:,:]*np.exp(2.*np.pi*1.j*k*xi) for k in range(M)],axis=0)
  p_xi = np.einsum('i...,i',A,np.array([np.exp(2.*np.pi*1.j*k*xi) for k in range(M)]))
  return p_xi / np.sqrt(M)

def trigPolyA(A,xi):
  """ Returns the trigonmetric polynomial p generated from A=fft(U) 
      evaluated at the point xi. Here p is given by
         p(xi) = \sum_{k} A_k exp(2*\pi*i*k*xi)
      where A=(A_k) is the Fourier transform of the ensemble U=(U_k).

      Parameters
      ----------
         A : np.array 
           A matrix ensemble which is the Fourier transform of an ensemble U.
         xi : float
            The point at which the polynomial is to be evaluated.

      Returns
      ---------
         p_xi : np.array
              The value of the trigonmetric polynomial of U at xi.
  """
  M    = A.shape[0]
  p_xi = np.einsum('i...,i',A,np.array([np.exp(2.*np.pi*1.j*k*xi) for k in range(M)]))
  return p_xi / np.sqrt(M)

def modulate(U,shift):
  """ Generate a matrix ensemble by applying the modulation operator with a specified
      shift to the ensemble U.

      Parameters
      ----------
         U : np.array
           A matrix ensemble. 
         shift : float
               The modulation operator will use this shift.

      Returns
      ----------
         V : np.array
           The matrix ensemble obtained after modulation with the specified shift.

  """
  V = np.zeros_like(U,dtype="complex")
  M = U.shape[0]
  A = myfft(U)
  for j in range(M):
    V[j,:,:] = trigPolyA(A,float(j)/M+shift)
  return V

def nearestUnitary(X):
  """ Computes a nearest orthogonal matrix to X using an SVD in the Frobenius norm.

      Parameters
      ----------
         X : np.array
           A matrix.

      Returns
      ----------
         V : np.array
           An orthogonal matrix nearest to X.

  """
  #if 'use_mpmath' in dir(nearestUnitary) and nearestUnitary.use_mpmath == True:
  #  u, _, vh = mp.svd(mp.matrix(X),compute_uv=True)
  #  return np.array((u*vh).tolist(),dtype="complex")
  #else:
  u, _, vh = np.linalg.svd(X)
  V = np.dot(u,vh)
  return V
  

def projU0(X):
  """ Computing the projection of a matrix X onto the first coordinate of the
      constraint C1. 

      Parameters
      ----------
         X : np.array
           A matrix. Typically this will be the first coordinate of a matrix
           ensemble U.

      Returns
      ----------
         V : np.array
           The projection of X onto the first coordinate of C1.

  """
  V      = np.zeros_like(X)
  V[0,0] = 1.
  V[1,1] = X[1,1] / max([abs(X[1,1]),1e-16])
  return V
   
def daub(d):
  """ Generates the matirx ensemble U associated with the 
      Daubechies wavelet of smoothness d.

      Parameters
      ----------
         d : int
           A positive integer representing the smoothness of the wavelet.

      Returns
      ----------
         U : np.array
           The matrix ensemble coresponding to the Daubechie wavelet of 
           smoothness d. 

  """
  x  = signal.daub(d+1) / np.sqrt(2)
  M  = len(x)
  y  = np.flip(x,axis=0) * np.array([(-1)**n for n in range(M)])
  m0 = np.zeros(M,dtype="complex")
  m1 = np.zeros(M,dtype="complex") 
  for j in range(M):
    m0[j] = sum( [x[k]*np.exp(2.j*np.pi*j*k/M) for k in range(M)] )
    m1[j] = sum( [y[k]*np.exp(2.j*np.pi*j*k/M) for k in range(M)] )
  U = np.zeros((M,2,2),dtype="complex")
  U[:,0,0] = m0
  U[:,0,1] = m1
  U[:,1,0] = np.roll(m0,M//2)
  U[:,1,1] = np.roll(m1,M//2)
  return U 


###############################################################
###        Functions used for processing the results        ###
###############################################################

def checkJCondition(U,EPS=1e-6):
  """ Numerically Checks if the matrix ensemble U satisifes the J-condition. 

      Parameters
      ----------
         U : np.array
           A matrix ensemble.
         EPS : float, optional
             The threshold to be used in the numerical check. If this is not
             specified, then 1e-6 is used.

      Returns
      ----------
         p  : bool
            A boolean variable which takes the value True if U passed the check,
            and False otherwise.   
  """
  M     = U.shape[0]
  sigma = np.array([[0,1],[1,0]])   
  JU    = np.matmul(sigma,U[:M//2,:,:])
  err   = np.linalg.norm(U[M//2:,:,:]-JU)
  p     = (err < EPS)
  return p

def checkOrthShift(U,EPS=1e-6):
  """ Numerically checks if the matrix ensemble U gives rise to orthogonal shifts.

      Parameters
      ----------
         U : np.array
           A matrix ensemble.
         EPS : float, optional
             The threshold to be used in the numerical check. If this is not
             specified, then 1e-6 is used.

      Returns
      ----------
         p  : bool
            A boolean variable which takes the value True if U passed the check,
            and False otherwise.
  """
  M          = U.shape[0]
  xi         = np.linspace(-0.25,0.25,num=2*M)
  m0         = np.array([trigPoly(U,x)[0,0] for x in xi])
  p          = np.min(abs(m0)) > EPS
  return p

def plotEnsemble(U,samples=100,filename=False):  
  """ Plots the trigonametric polynomial associated with matrix ensemble U.
      This function is used to generate the plots in the paper.

      Parameters
      ----------
         U : np.array
           Matrix ensemble used to generated the polynomial.
         samples : int, optional
           The number of samples pointed used.
         filename : str, optional
             If specificed, the plot will saved using the specified
             filename, else the plot will just be displayed.

  """
  
  xi = np.linspace(0.0,1.0,num=samples)
  m = np.array([trigPoly(U,x)[:,0] for x in xi])
  plt.figure(figsize=(20,10))
  for j in range(2):
    plt.subplot(1,2,j+1)
    plt.plot(xi,m[:,j].real)
    plt.plot(xi,m[:,j].imag,dashes=[4,2])
    plt.plot(xi,abs(m[:,j]),dashes=[2,2])
    plt.xlabel(r"$\xi$")
    plt.legend(["$\Re(m_"+str(j)+")$","$\Im(m_"+str(j)+")$","$|m_"+str(j)+"|$"])
  if not filename:
    plt.show()
  else:
    plt.savefig(filename)
  plt.close()

def plotWavelet(x,phi,psi,filename=False):
  """ Plots the scaling and wavelet functions. This function is
      used to generate the plots in the paper.
      
      Parameters
      ----------
         x : np.array
           The horizontal (x-axis) for plotting.
         phi : np.array
            The scaling function values at x.
         psi : np.array
             The wavelet function's values at x.
         filename  : str, optional
             If specificed, the plot will saved using the specified
             filename, else the plot will just be displayed.
  """
  imagPart = np.linalg.norm(phi.imag) + np.linalg.norm(psi.imag)
  if np.isclose(imagPart,0,atol=1e-6):
    plt.figure(figsize=(5,5))
    plt.plot(x,phi.real)
    plt.plot(x,psi.real,dashes=[4,2])
    plt.xlabel("$t$")
    plt.legend(["$\phi(t)$","$\psi(t)$"])
  else: 
    plt.figure(figsize=(20,10))
    plt.subplot(1,2,1)
    plt.plot(x,phi.real)
    plt.plot(x,phi.imag,dashes=[4,2])
    plt.xlabel("$t$")
    plt.legend(["$\Re\phi(t)$","$\Im\phi(t)$"])
    plt.subplot(1,2,2)
    plt.plot(x,psi.real)
    plt.plot(x,psi.imag,dashes=[4,2])
    plt.xlabel("$t$")
    plt.legend(["$\Re\psi(t)$","$\Im\psi(t)$"])
  if not filename:
    plt.show()
  else:
    plt.savefig(filename)
  plt.close()



###############################################################
###               Cascade algorithm methods                 ###
###############################################################
# The code for these methods was provided by Neil Dizon

def cascade(h,g):
  phi   = cascadeSkeleton( h )
  phi,x = cascadePhi(phi, h, 7)
  psi   = cascadePsi(phi,x,g)
  return x,phi,psi

def ensembleToCoeff(U):
  """ Extracts the cofficients for the FIR low-pass filter producing
      wavelets from a matrix ensemble U.

      Args: 
         U (np.array): a matrix ensemble.
      
      Returns:
         h (np.array): the coefficents. 
         g (np.array): the coefficents. 
  """
  m0 = U[:,0,0]
  m1 = U[:,0,1]
  M  = U.shape[0]
  #d  = M//2-1
  h  = np.zeros(M,dtype="complex")
  g  = np.zeros(M,dtype="complex")
  for j in range(M):
    h[j] = sum( [m0[k]*np.exp(-2.j*np.pi*j*k/M) for k in range(M)] )
    g[j] = sum( [m1[k]*np.exp(-2.j*np.pi*j*k/M) for k in range(M)] )
  h = 2*h/M
  g = 2*g/M
  return h,g

def cascadeSkeleton(a):
  """ Computes the skeleton for the cascade, i.e., the values of Phi at 0,1,...,M-1.
  
      Args: 
         a (np.array): the low-pass filter, or the coefficients in the scaling equation.
      
      Returns:
         skeleton (np.array): a vector that contains the values of Phi at 0,1,...,M-1.
  """
    #Let's check if we have the right dimensions.
  if (len(a.shape) == 1):
    a=a[np.newaxis,:]
  assert(len(a.shape) == 2 and a.shape[0]==1), 'input must be a 2d array with one row'

  M = np.shape(a)[1]
  B = np.zeros([M,3*M-2],dtype='complex')
  A = np.zeros([1,3*M-2],dtype='complex')
  A[0,0:M]=np.flip(a,axis=1)
  for j in range(M):
    B[j,:]=A
    A=np.roll(A,2)
  B=B[:,M-1:2*M-1]
  lamb, evec = np.linalg.eig(B)
  ind = np.argmin(np.absolute(lamb - 1))
  skeleton = evec[:,ind]
  skeleton = skeleton/np.sum(skeleton)
  return skeleton

def cascadePhi(Phi,h,level):
  """ Computes the values of Phi at the dyadic rationals up to 1/(2**level)
      
      Args: 
         h (np.array): the low-pass filter, or the coefficients in the scaling equation.
         Phi (np.array): the scaling function Phi valued at 0,1,...,M-1 from cascadeskeleton
         level (int): determines the fineness of values we want for Phi; up to 1/2**level

      Returns:
         Phi (np.array): a vector that contains the values of Phi at the dyadic rationals
         X (np.array): a vector that contains the dyadic rationals corresponding to Phi
  """
  if (len(Phi.shape) == 1):
    Phi=Phi[np.newaxis,:]
  assert(len(Phi.shape) == 2 and Phi.shape[0]==1), 'input must be a 2d array with one row'
  M = Phi.shape[1]
  X = np.arange(M)[np.newaxis,:]
  
  for count in range(level):
    step = count + 1

    Xnew = X[0,1:] - 1/(2**step)
    Xnew = Xnew[np.newaxis,:]
    Phinew = np.zeros([1,Xnew.shape[1]], dtype = 'complex')

    for k in range(Xnew.shape[1]):
      Phinew[0,k] = sum(h[j]*Phi[0,np.nanargmin(abs( X - 2*Xnew[0,k] + j))] for j in range(M))
      #np.nanargmin works only when Phi[0,0] = 0.

    X=np.concatenate((X,Xnew),axis=1)
    Phi=np.concatenate((Phi,Phinew),axis=1)

  Z=np.concatenate((X,Phi),axis=0)
  Z=Z[:,np.argsort(Z[0,:])]

  X=np.real(Z[0,:])
  Phi = Z[1,:]
  return Phi,X

def cascadePsi(Phi,X,g):
  """ Computes the values of Psi at the dyadic rationals up to same level as Phi
      
      Args: 
         g (np.array): coefficients in the wavelet equation, or the high-pass filter
         Phi (np.array): contains the values of the scaling function Phi
         X (np.array): the points where Phi are evaluated

      Returns:
         Psi (np.array): a vector that contains the values of Psi   
  """
    #Let's check if we have the right dimensions.
  if (len(Phi.shape) == 1):
    Phi=Phi[np.newaxis,:]
  assert(len(Phi.shape) == 2 and Phi.shape[0]==1), 'input must be a 2d array with one row'
  if (len(X.shape) == 1):
    X=X[np.newaxis,:]
  assert(len(X.shape) == 2 and X.shape[0]==1), 'input must be a 2d array with one row'
  
  M = g.shape[0]
  m = Phi.shape[1]
  Psi = np.zeros([1,m], dtype = 'complex')

  for k in range(m):
    Psi[0,k] = sum(g[j]*Phi[0,np.nanargmin(abs( X - 2*X[0,k] + j))] for j in range(M))
    #np.nanargmin works only when Phi[0,0] = 0.
  return Psi[0]

def printCoeff( h ):
  """Print the entries of the vector h.
  
  Parameters
  ----------
     h : array_like
       The array whose entries are to be printed.
 
  """
  print( "Coefficients of h are:" )
  for hi in h:
    print( "   ", hi)


###############################################################
###           A example of the method in action             ###
###############################################################

def paperExperiments(directory):
  """ Running this method produces the paper's results for the 1D problem. """  

  plt.style.use('grayscale')
  repetitions = 10
  parameters  = [(4,1),(6,2),(8,3),(10,4),(12,5),(14,6)] 
  start_seed  = 1951

  for (M,d) in parameters:
    s = start_seed
    #recall that stats is of the form stats = [it, err, t, err<EPS]
    iters, error, time, solved = [],[],[],[]
    for r in range(repetitions):
      s += 1
      print("#"*47)
      print( "Problem dimensions are (M,d) = (" +str(M) + "," + str(d) + ")")
      print( "Initial Point Seed:", s )
      np.random.seed(s)
      U0 = PC1( 2.*np.random.rand(M,2,2)-1. + (2.*np.random.rand(M,2,2)-1.)*1.j )
      U,stats  = DR(M,d,EPS=1e-3,MAX_ITER=1e6,initialU=U0)
      h,g  = ensembleToCoeff(U)
      printCoeff( h )  
      x, phi, psi = cascade( h, g )

      iters.append(stats[0])
      error.append(stats[1])
      time.append(stats[2])
      solved.append(stats[3])

      name = "s"+str(s)


      plotWavelet(x,phi,psi,filename=directory+"/1D-M"+str(M)+"d"+str(d)+"-"+name+"-wavelet.png")
      plotEnsemble(U,filename=directory+"/1D-M"+str(M)+"d"+str(d)+"-"+name+"-trigpoly.png")
      plotWavelet(x,phi,psi,filename=directory+"/1D-M"+str(M)+"d"+str(d)+"-"+name+"-wavelet.pdf")
      plotEnsemble(U,filename=directory+"/1D-M"+str(M)+"d"+str(d)+"-"+name+"-trigpoly.pdf")
      io.savemat(directory+"/1D-M"+str(M)+"d"+str(d)+"-"+name+"-ensemble.mat",{'U':U})

      print("")
   

    print( "#"*47 )  
    print( "#"*14 + "  Instance Summary  " + "#"*13 )  
    print( "  Dimensions are (M,d)  = ("+str(M)+","+str(d)+")" )

    print( "  Instances solved      =", sum(solved), "of" , repetitions )
    iters = list(compress(iters,solved))
    if iters != []:
      print( "  Mean (max) iterations = %.3f (%.3f)" % (np.mean(iters), np.max(iters)) )
    time  = list(compress(time,solved))
    if time != []:
      print( "  Mean (max) time       = %.3f (%.3f)" % (np.mean(time), np.max(time)) )
    print( "#"*47 )  
    print( "#"*47 )  
    print( "\n\n\n" )

def main():
  """This method is an example of how to use this software."""
  parameters  = [(4,1),(6,2)]
  for (M,d) in parameters:
    print("#"*47)
    print( "Problem dimensions are (M,d) = (" +str(M) + "," + str(d) + ")")
    U,_  = DR(M,d,EPS=1e-9)
    h,g  = ensembleToCoeff(U)
    printCoeff( h )  
    x, phi, psi = cascade( h,g )
    plotWavelet(x,phi,psi)
    plotEnsemble(U)
    print("")

if __name__ == '__main__':
  if len(sys.argv) > 1 and sys.argv[1] == "-e":
    directory = "Results1D-" + datetime.date.today().strftime("%b%d")
    if not os.path.exists(directory):
      os.makedirs(directory)
    paperExperiments(directory)
  else:
    main()


